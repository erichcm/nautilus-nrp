extern crate gio;
extern crate gtk;
extern crate glib;

use gio::prelude::*;
use gtk::prelude::*;

use std::env::args;
use std::path::PathBuf;
use std::process::Command;
use std::io;

enum ProjectType {
    Binary,
    Library,
}



fn create_rust_project(kind: ProjectType, dir: &PathBuf, project: &str) -> Result<(), io::Error>{
    let kind_arg = match kind {
        ProjectType::Binary => "--bin",
        ProjectType::Library => "--lib",
    };

    Command::new("cargo")
        .arg("new")
        .arg(kind_arg)
        .arg(project)
        .current_dir(dir.to_str().unwrap())
        .spawn()
        .expect("cargo command failed!");

    Ok(())
}

fn create_and_show_dialog<F: Fn() + 'static>(parent: &gtk::ApplicationWindow,
                                             title: &str,
                                             msg: &str,
                                             func: F) {

    let dialog = gtk::Dialog::new_with_buttons(Some(title),
                                               Some(parent),
                                               gtk::DialogFlags::MODAL,
                                               &[("Close", gtk::ResponseType::Close)]);
    dialog.set_default_response(gtk::ResponseType::Close);
    let label = gtk::Label::new(Some(msg));
    let dialog_content_area = dialog.get_content_area();
    dialog_content_area.pack_start(&label, true, true, 1);
    dialog.connect_response(move |dialog, _| {
        dialog.destroy();
        func();
    });

    dialog.show_all();
}

fn create_btn_clicked(app: &gtk::Application,
                      window: &gtk::ApplicationWindow,
                      combo: &gtk::ComboBoxText,
                      project: &gtk::Entry,
                      dir: &PathBuf) {
    let project_name = project.get_text().unwrap();
    if project_name.as_str() == "" {
        create_and_show_dialog(&window,
                               "Missing project name",
                               "Please provide a project name",
                               || ());
        return ();
    }


    if let Some(text) = combo.get_active_text() {
        let res = match text.as_str() {
            "Binary" => create_rust_project(ProjectType::Binary,
                                            &dir, &project_name),
            "Library" => create_rust_project(ProjectType::Library,
                                             &dir, &project_name),
            _ => unreachable!(),
        };

        if res.is_ok() {
            let a = app.clone();
            let f = move || a.quit();

            create_and_show_dialog(&window,
                                   "Project created",
                                   "Project created successfully",
                                   f);
        }

    }
}

fn build_ui(application: &gtk::Application, dir: std::path::PathBuf) {
    // Create and configure window.
    let window = gtk::ApplicationWindow::new(application);
    window.set_title("Create new Rust project");
    window.set_border_width(5);
    window.set_position(gtk::WindowPosition::Center);
    window.set_default_size(350, 50);


    let hbox = gtk::Box::new(gtk::Orientation::Vertical, 1);
    let label = gtk::Label::new(Some("Write project name and select type."));

    let box_btn = gtk::ButtonBox::new(gtk::Orientation::Horizontal);
    let create_btn = gtk::Button::new_with_label("Create");
    let cancel_btn = gtk::Button::new_with_label("Cancel");

    box_btn.set_layout(gtk::ButtonBoxStyle::End);
    box_btn.pack_start(&cancel_btn, true, true, 1);
    box_btn.pack_start(&create_btn, true, true, 1);

    let project_name = gtk::Entry::new();

    let combo = gtk::ComboBoxText::new();
    combo.append_text(&"Binary");
    combo.append_text(&"Library");
    combo.set_active(Some(0));

    let combo_clone = combo.clone();
    let project_name_clone = project_name.clone();
    let application_clone = application.clone();
    let window_clone = window.clone();
    create_btn.connect_clicked(move |_| {
        create_btn_clicked(&application_clone,
                           &window_clone,
                           &combo_clone,
                           &project_name_clone,
                           &dir);
    });

    let application = application.clone();
    cancel_btn.connect_clicked(move |_| {
        application.quit();
    });

    hbox.pack_start(&label, false, false, 5);
    hbox.pack_start(&project_name, false, false, 5);
    hbox.pack_start(&combo, false, false, 5);
    hbox.pack_start(&box_btn, false, false, 5);
    window.add(&hbox);

    window.show_all();
}

fn main () -> Result<(), io::Error> {
    let app = gtk::Application::new (Some("com.github.ericho.nautilus-nrp"),
            gio::ApplicationFlags::HANDLES_OPEN)
        .expect("Initialization failed...");

    let app_args = args().collect::<Vec<_>>().clone();
    if app_args.len() != 2 {
        return Err(io::Error::new(io::ErrorKind::Other, "Wrong arguments."));
    }

    app.connect_open(|a, files, _| {
        if let Some(path) = files[0].get_path() {
            build_ui(a, path);
        }
        ()
    });

    app.run(&args().collect::<Vec<_>>());

    Ok(())
}
