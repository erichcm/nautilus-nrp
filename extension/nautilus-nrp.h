#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define NAUTILUS_TYPE_NRP (nautilus_nrp_get_type ())

G_DECLARE_FINAL_TYPE (NautilusNrp, nautilus_nrp, NAUTILUS, NRP, GObject)

void nautilus_nrp_load (GTypeModule *module);

G_END_DECLS
