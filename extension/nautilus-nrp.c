#include <config.h>
#include <string.h>
#include <glib/gi18n-lib.h>
#include <nautilus-extension.h>
#include <nautilus-nrp.h>

struct _NautilusNrp
{
    GObject parent_instance;

    gboolean cargo_present;
};

static void menu_provider_iface_init (NautilusMenuProviderInterface *iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED (NautilusNrp, nautilus_nrp, G_TYPE_OBJECT, 0,
                                G_IMPLEMENT_INTERFACE_DYNAMIC (NAUTILUS_TYPE_MENU_PROVIDER,
                                                               menu_provider_iface_init))

static gboolean
is_only_one_item(GList *l)
{
    if (l == NULL)
    {
      return FALSE;
    }

    return (l != NULL && l->next == NULL);
}

static void
new_rust_project_callback (NautilusMenuItem *item,
                           gpointer          user_data)
{

    g_autoptr (GString) command = NULL;
    gchar *dir = NULL;

    dir = g_object_get_data (G_OBJECT (item), "dir");
    command = g_string_new ("nautilus-nrp");

    g_string_append_printf (command, " \"%s\"", dir);

    g_spawn_command_line_async (command->str, NULL);
}

static GList *
get_file_items (NautilusMenuProvider *provider,
                GtkWidget            *window,
                GList                *files)
{
    GList *items = NULL;
    NautilusMenuItem *item;
    NautilusNrp *nrp;
    gchar *uri = NULL;

    nrp = NAUTILUS_NRP (provider);
    if (!nrp->cargo_present) return NULL;
    if (files == NULL) return NULL;

    if (nautilus_file_info_is_directory ((NautilusFileInfo *) files->data)
        && is_only_one_item(files)) {

        item = nautilus_menu_item_new ("NautilusTest::newcargo",
                                       _("New Rust project..."),
                                       _("Create new Rust project here"),
                                       "new-rust");

        g_signal_connect (item,
                          "activate",
                          G_CALLBACK (new_rust_project_callback),
                          provider);

        uri = nautilus_file_info_get_uri ((NautilusFileInfo *) files->data);

        g_object_set_data_full (G_OBJECT (item),
                                "dir",
                                uri,
                                g_free);

        items = g_list_append (items, item);
    }


    return items;
}

static GList *
get_background_items (NautilusMenuProvider *provider,
                      GtkWidget            *window,
                      NautilusFileInfo     *current_folder)
{
    NautilusMenuItem *item;
    NautilusNrp *nrp;
    GList *items = NULL;
    gchar *uri = NULL;

    nrp = NAUTILUS_NRP (provider);

    if (!nrp->cargo_present) return NULL;
    if (current_folder == NULL) return NULL;

    item = nautilus_menu_item_new ("NautilusNRP::newcargo",
                                   _("New Rust project..."),
                                   _("Create new Rust project here"),
                                   "new-rust");

    g_signal_connect (item,
                      "activate",
                      G_CALLBACK (new_rust_project_callback),
                      provider);

    uri = nautilus_file_info_get_uri (current_folder);

    g_object_set_data_full (G_OBJECT (item),
                            "dir",
                            uri,
                            g_free);

    items = g_list_append (items, item);
    return items;
}

static void
menu_provider_iface_init (NautilusMenuProviderInterface *iface)
{
    iface->get_file_items = get_file_items;
    iface->get_background_items = get_background_items;
}

static void
nautilus_nrp_init (NautilusNrp *nrp)
{
    nrp->cargo_present = (g_find_program_in_path ("cargo") != NULL);
}

static void
nautilus_nrp_class_init (NautilusNrpClass *klass)
{
}

static void
nautilus_nrp_class_finalize (NautilusNrpClass *klass)
{
}

void
nautilus_nrp_load (GTypeModule *module)
{
    nautilus_nrp_register_type (module);
}
