#include <config.h>
#include <nautilus-extension.h>
#include <glib/gi18n-lib.h>
#include "nautilus-nrp.h"

void
nautilus_module_initialize (GTypeModule *module)
{
    nautilus_nrp_load (module);

    bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
    bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
}

void
nautilus_module_shutdown (void)
{
}

void nautilus_module_list_types (const GType **types,
                                 int          *num_types)
{
    static GType type_list[1];

    type_list[0] = NAUTILUS_TYPE_NRP;
    *types = type_list;

    *num_types = 1;
}
