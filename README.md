# Nautilus New Rust Project extension

A simple Nautilus extension to create a Rust project in the selected location.

# Building

## Requirements

To build this extension the Nautilus development package is required. In Fedora this can be installed with:

```
$ sudo dnf install nautilus-devel
```

Also, the `meson` build system is required.

Ensure that `cargo` is present in `PATH` as the plugin will load only if `cargo` is installed.

## Build

To build the nautilus extension just run: 

```
$ meson builddir
$ ninja -C builddir
```

Additionally, the `nautilus-nrp` shall be compiled. For this run: 

```
$ cargo build --release
```

## Install

The Nautilus extensions are located in `/usr/lib64/nautilus/extension-3.0`, to install this extension run:

```
$ sudo cp builddir/nautilus-nrp.so /usr/lib64/nautilus/extension-3.0
```

and restart Nautilus.

The `nautilus-nrp` should be placed on any folder pointed by `$PATH`, for example `/usr/local/bin`.

```
$ sudo cp target/release/nautilus-nrp /usr/local/bin
```

